const config = require('./config.json');
const data = require('./data.json');

const logger = require('./logger');

const Discord = require('discord.js');
const client = new Discord.Client();

logger.phase("INITIALIZATION");

client.once('ready', () => {
	logger.statusLog("CLIENT", "connection", "OK");
	logger.phase("RUNNING");
	logger.botConnectedLog(config.bot.botName, client.user.username);
});

client.on('message', message => {
	if(message.author.bot) return;
	if(message.content.indexOf(config.commands.prefix) !== 0) return;
	const args = message.content.slice(config.commands.prefix.length).trim().split(/ +/g);
	const command = args.shift().toLowerCase();

	switch(command) {
		case("help"):
				message.author.send("Commande disponible : \n-?ping : calcule la latence avec moi\n-?blush\n-?pat quelqu'un\n-?poke quelqu'un\n-?deepthroat quelqu'un\-?nohomo");
				break;
		case("ping"):
				message.channel.send("Ping?").then((m)=>{
					m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`);
				});
				break;
		case("say"):
				const sayMessage = args.join(" ");
				message.delete().catch(O_o=>{});
				message.channel.send(sayMessage);
				break;
		case("blush"):
				var r = (Math.floor(Math.random()*data.blush.blush_number));
				message.channel.send(data.blush.blush_data[r]);
				break;
		case("deepthroat"):
				var r = (Math.floor(Math.random()*data.deepthroat.deepthroat_number));
				message.channel.send("**"+message.member.toString()+"** want to give a deepthroat !\n"+data.deepthroat.deepthroat_data[r]);
				break;
		case("poke"):
				var r = (Math.floor(Math.random()*data.poke.poke_number));
				message.channel.send("**"+args[0]+"**,poke in your face !\n"+data.poke.poke_data[r]);
				break;
		case("pat"):
				var r = (Math.floor(Math.random()*data.pat.pat_number));
				message.channel.send("**"+args[0]+"**, you got a pat from **"+message.member.toString()+"**\n"+data.pat.pat_data[r]);
				break;
		case("nohomo"):
				message.channel.send(":star: no homo :star: \n" +"\nhttp://i3.kym-cdn.com/photos/images/newsfeed/000/917/277/74e.gif");
				break;
		default:
	}
});

client.login(config.bot.botToken);
