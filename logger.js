const logConfig = require("./config.json").logger;

exports.statusLog = function(src, element, status) {
	console.log("[" + formatSource(src) + "] : " + formatElement(element) + " : " + formatStatus(status))
}

exports.messageLog = function(time, channel, author) {
	console.log("[" + time + "] " + channel + " : " + author);
}

exports.commandLog = function(args, success) {
	var arguments = args.join(" ");
	console.log("\tCommand: " + arguments + " : " + success);
}

exports.botConnectedLog = function(botName, userName) {
	console.log(botName + " connected as " + userName);
}

exports.phase = function(phase) {
	console.log("\n" + phase/*.padEnd(30, "=")*/);
}

exports.debugLog = function(src, content) {
	if (logConfig.debug)
		console.log("[" + formatSource(src) + "] : " + content);
}

function formatSource(src) {
	return src/*.padStart(logConfig.srcLength, " ")*/;
}
function formatElement(element) {
	return element/*.padStart(logConfig.elemLength, " ")*/;
}
function formatStatus(status) {
	return status/*.padStart(logConfig.statusLength, " ")*/;
}
